import { Router } from "express";
import { createUser, login } from "../services/userService.js";

const router = Router();

router.post("/create", async (req, res) => {
    const result = await createUser(req.body);
    res.status(result ? 201 : 400).send();
});

router.post("/login", async (req, res) => {
    const token = await login(req.body);
    res.status(token ? 200 : 401).json({ token });
});

export default router;