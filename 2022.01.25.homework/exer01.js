/**
Create an express REST-API, which offers CRUD for your books.
Object Book = { id: number, name: string, author: string, read: boolean }
GET /api/v1/books → Returns a list of all the books
GET /api/v1/books/{id} → Returns a book with a corresponding ID.
POST /api/v1/books → Creates a new book.
PUT /api/v1/books/{id} → Modifies an existing book
DELETE /api/v1/books/{id} Removes a book with a corresponding id
Note: You don’t need to enable an actual Database at this point. Simple runtime-cache (For
example: let books = []) will do for now. Do not save to a file, as we might end up ‘clouding’
this application later…
 */

// Postman : GET http://localhost:3000/api/v1/books
// list of all books
// Postman : GET http://localhost:3000/api/v1/books/:id
// Returns a book with a corresponding ID
// Postman : POST http://localhost:3000/api/v1/books/
// Creates a new book
// Postman : PUT http://localhost:3000/api/v1/books/:id
// Modifies an existing book
// Postman : DELETE /api/v1/books/:id
// Removes a book with a corresponding id

import express from "express";

const app = express();
app.use (express.json()); // this must be used to parse POST request body made in JSON format (se in Postman)

const port = 3000;

const keys = ["id", "name", "author","read"];
const books = [
    {"id": 0, "name" : "Robinson Crusoe", "author" : "Daniel Defoe", "read": true},
    {"id": 1, "name" : "Count of Monte Cristo", "author" : "Alexandre Dumas", "read": true}  
]   ;


// GET /api/v1/books → Returns a list of all the books
app.get("/api/v1/books", (req,res) => {
    
    if (books.length !== 0) {
        res.status(200).send(books);
    } else {
        res.status(400).send("No books founds.");
    }
});

// GET /api/v1/books/:id → Returns a book with a corresponding ID
app.get("/api/v1/books/:id", (req,res) => {
    
    const reqID = Number(req.params.id);

    let returnData;

    books.find (   (value,index,array) => {
        if (array[index].id === reqID) {
            returnData = array[index];
        }}
    );

    if (returnData !== undefined) {
        res.status(200).send(returnData);
    } else {
        res.status(400).send(`No book with ID ${reqID} found.`);
    }
});


// POST /api/v1/books → Creates a new book
app.post("/api/v1/books", (req,res) => {
    
    const book = req.body;
    if (!keys.every(key => book.hasOwnProperty(key))) {
        res.status(400).send(`Please provide following data : ${keys}.`); 
    } else {
        books[books.length] = book;
        
        console.log(books);
    
        res.status(200).send("A new book saved.");    
    }

});

// PUT /api/v1/books/{id} → Modifies an existing book
app.put("/api/v1/books/:id", (req,res) => {
    
    const reqID = Number(req.params.id);

    const book = req.body;
    let bookFound = false;
    let incorrectData = false;
    
    if (!keys.every(key => book.hasOwnProperty(key))) {
        incorrectData = true;
    } else {
        console.log(`Requested id : ${reqID}.`);
        books.find (   (value,index,array) => {
            if (array[index].id === reqID) {
                bookFound = true;
                books[index] = book;
            }}
        );
    }

    if (incorrectData) {
        res.status(400).send(`Please provide following data : ${keys}.`); 
    } else if (bookFound) {
        res.status(200).send(`Book with ID ${reqID} updated.`);
    } else {
        res.status(400).send(`Book with ID ${reqID} not found.`);
    }
    
    console.log(books);
});

// DELETE /api/v1/books/{id} 
// Removes a book with a corresponding id
app.delete("/api/v1/books/:id", (req,res) => {
    
    const reqID = Number(req.params.id);

    let delIndex = undefined;
    books.find (   (value,index,array) => {
        if (array[index].id === reqID) {
            delIndex = index;
        }}
    );
    if (delIndex !== undefined) {
        books.splice(delIndex,1);
        res.status(200).send(`Book record with ID ${reqID} deleted.`);    
    } else {
        res.status(400).send(`Book record with ID ${reqID} not found.`);    
    }
    
    console.log(books);
});

app.listen(port,  ()  => {
    console.log (`Listening to port ${port}`);
});
