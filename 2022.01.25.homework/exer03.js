/**
Create an express REST-API, which offers CRUD for your books.
Object Book = { id: number, name: string, author: string, read: boolean }
GET /api/v1/books → Returns a list of all the books
GET /api/v1/books/{id} → Returns a book with a corresponding ID.
POST /api/v1/books → Creates a new book.
PUT /api/v1/books/{id} → Modifies an existing book
DELETE /api/v1/books/{id} Removes a book with a corresponding id
Note: You don’t need to enable an actual Database at this point. Simple runtime-cache (For
example: let books = []) will do for now. Do not save to a file, as we might end up ‘clouding’
this application later…

Improve your application with the Middleware & Libraries introduced in the lecture slides.
- install nodemon as a DEV-DEPENDENCY and use it in local development.
 npm install nodemon --save-dev
 modify : package.json
     "dev": "nodemon 2022.01.25.homework/exer02.js"
 start with:
 npm run dev

- Create and take 404 and errorhandler -middlewares into use.
- BONUS STEP: Create a middleware for logging in the application. Make the logs
readable. req-object contains a lot of needed data for this. Google ‘logging
middleware for express’. Do not use any external packages for this.
- BONUS STEP 2: Add helmet to your application’s dependencies and create a basic
setup for security. https://github.com/helmetjs/helmet

3. EXTRA-BONUS
Make your application serve static HTML frontend. If you have a lot of time and nothing to
do, make the frontend pretty


// Postman : GET http://localhost:3000/api/v1/books
// list of all books
// Postman : GET http://localhost:3000/api/v1/books/:id
// Returns a book with a corresponding ID
// Postman : POST http://localhost:3000/api/v1/books/
// Creates a new book
// Postman : PUT http://localhost:3000/api/v1/books/:id
// Modifies an existing book
// Postman : DELETE /api/v1/books/:id
// Removes a book with a corresponding id



**/


import express from "express";
import fs from "fs"; // for logging
import chalk from "chalk"; // for colourer display
import helmet from "helmet"; // for security

import path from "path";
import {fileURLToPath} from "url";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();
app.use (express.json()); // this must be used to parse POST request body made in JSON format (se in Postman)

const port = 3000;

const keys = ["id", "name", "author","read"];
const books = [
    {"id": 0, "name" : "Robinson Crusoe", "author" : "Daniel Defoe", "read": true},
    {"id": 1, "name" : "Count of Monte Cristo", "author" : "Alexandre Dumas", "read": true}  
]   ;

// enable all helmet security policies, refer to https://github.com/helmetjs/helmet
// details on effects : https://blog.logrocket.com/express-middleware-a-complete-guide/#helmet
// e.g. removed of X-Powered-By from headers
app.use(helmet());


let logger = (req, res, next) => { 
    let current_datetime = new Date();
    let formatted_date =
      current_datetime.getFullYear() +
      "-" +
      (current_datetime.getMonth() + 1) +
      "-" +
      current_datetime.getDate() +
      " " +
      current_datetime.getHours() +
      ":" +
      current_datetime.getMinutes() +
      ":" +
      current_datetime.getSeconds();
    let method = req.method;
    let url = req.url;
    let status = res.statusCode;
    const start = process.hrtime();
    const durationInMilliseconds = getActualRequestDurationInMilliseconds(start);
    // let log = `[${formatted_date}] ${method}:${url} ${status} ${durationInMilliseconds.toLocaleString()} ms`;
    let log = `[${chalk.blue(formatted_date)}]  ${method}:${url} ${status} ${chalk.red(durationInMilliseconds.toLocaleString() + "ms")}`;
    console.log(log);
    fs.appendFile("request_logs.txt", log + "\n", err => {
        if (err) {
            console.log(err);
        }
    });
    next();
};

// Start 
app.use(logger);

// GET /api/v1/books → Returns a list of all the books
app.get("/api/v1/books", (req,res) => {
    
    if (books.length !== 0) {
        res.status(200).send(books);
    } else {
        res.status(400).send("No books founds.");
    }
});

// GET /api/v1/books/:id → Returns a book with a corresponding ID
app.get("/api/v1/books/:id", (req,res) => {
    
    const reqID = Number(req.params.id);

    let returnData;

    books.find (   (value,index,array) => {
        if (array[index].id === reqID) {
            returnData = array[index];
        }}
    );

    if (returnData !== undefined) {
        res.status(200).send(returnData);
    } else {
        res.status(400).send(`No book with ID ${reqID} found.`);
    }
});


// POST /api/v1/books → Creates a new book
app.post("/api/v1/books", (req,res) => {
    
    const book = req.body;
    if (!keys.every(key => book.hasOwnProperty(key))) {
        res.status(400).send(`Please provide following data : ${keys}.`); 
    } else {
        books[books.length] = book;
        
        console.log(books);
    
        res.status(200).send("A new book saved.");    
    }

});

// PUT /api/v1/books/{id} → Modifies an existing book
app.put("/api/v1/books/:id", (req,res) => {
    
    const reqID = Number(req.params.id);

    const book = req.body;
    let bookFound = false;
    let incorrectData = false;
    
    if (!keys.every(key => book.hasOwnProperty(key))) {
        incorrectData = true;
    } else {
        console.log(`Requested id : ${reqID}.`);
        books.find (   (value,index,array) => {
            if (array[index].id === reqID) {
                bookFound = true;
                books[index] = book;
            }}
        );
    }

    if (incorrectData) {
        res.status(400).send(`Please provide following data : ${keys}.`); 
    } else if (bookFound) {
        res.status(200).send(`Book with ID ${reqID} updated.`);
    } else {
        res.status(400).send(`Book with ID ${reqID} not found.`);
    }
    
    console.log(books);
});

// DELETE /api/v1/books/{id} 
// Removes a book with a corresponding id
app.delete("/api/v1/books/:id", (req,res) => {
    
    const reqID = Number(req.params.id);

    let delIndex = undefined;
    books.find (   (value,index,array) => {
        if (array[index].id === reqID) {
            delIndex = index;
        }}
    );
    if (delIndex !== undefined) {
        books.splice(delIndex,1);
        res.status(200).send(`Book record with ID ${reqID} deleted.`);    
    } else {
        res.status(400).send(`Book record with ID ${reqID} not found.`);    
    }
    
    console.log(books);
});

// Static main page : /public/index.html
// order is important, do not put routing after this
app.use(express.static(__dirname +"/public"));
app.get("/",(_req,res) => {
    res.status(200).send("OK!");
});


const unknownEndpoint = (_req, res) => {
    res.status(404).send({ error: "No one here" });
};

const errorHandler = (error, _req, res, next) => {

    console.error(error.message);

    next(error);
};

const getActualRequestDurationInMilliseconds = start => {
    const NS_PER_SEC = 1e9; //  convert to nanoseconds
    const NS_TO_MS = 1e6; // convert to milliseconds
    const diff = process.hrtime(start);
    return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS;
};




app.use(unknownEndpoint);
app.use(errorHandler);

app.listen(port,  ()  => {
    console.log (`Listening to port ${port}`);
});
