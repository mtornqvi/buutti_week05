import {sumTwo} from "../src/index.js";

test("Sum returns 4 with parameters 1 and 3", () => {
    const result = sumTwo(1,3);
    expect(result).toBe(4);
});

// must use expect with callback function
test("Sum throws an error with string parameter ", () => {
    expect(() => {
        sumTwo(4, "raimo");
    }).toThrow();
});