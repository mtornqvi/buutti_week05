export const sumTwo = (a,b) => {
    if (typeof a !== "number" || typeof b !== "number") {
        throw new Error ("Either one or both parameters are invalid : {a} ja {b}");
        
    }

    return a +b;
};


    