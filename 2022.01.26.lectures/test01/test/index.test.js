import sumTwo from "../src/index.js";

// Happy case
test("Sum returns 4 with parameters 1 and 3", () => {
    const result = sumTwo(1,3);
    expect(result).toBe(4);
}
);