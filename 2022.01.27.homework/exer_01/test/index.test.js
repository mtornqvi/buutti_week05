import request from "supertest";
import index from "../src/index.js";

describe("Index endpoints", () => {

    // GET
    // Happy case #1 : all books
    it("Test : /api/v1/books", (done) => {
        request(index)
            .get("/api/v1/books")
            .expect(200, done);
    });    

    // GET
    // Happy case #2
    it("Test : /api/v1/books/1", (done) => {
        request(index)
            .get("/api/v1/books/1")
            .expect(200, done);
    });    

    // Expect not found
    it("Test : /api/v1/books/4", (done) => {
        request(index)
            .get("/api/v1/books/4")
            .expect(400, done);
    });    

    // POST
    // Happy case #3
    it("Post : /api/v1/books/", (done) => {
        request(index)
            .post("/api/v1/books/")
            .send( {
                "id" : 5,
                "name" : "Mustanaamio",
                "author" : "mikko",
                "read": true
            } )
            .expect(200, done);
    });    

    // Incorrect parameters
    it("Post : /api/v1/books/", (done) => {
        request(index)
            .post("/api/v1/books/")
            .send( {
                "id" : 5,
                "name" : "Mustanaamio",
                "author" : "mikko"
            } )
            .expect(400, done);
    });   

    // PUT
    // Happy case #4
    it("Put : /api/v1/books/1", (done) => {
        request(index)
            .put("/api/v1/books/1")
            .send( {
                "id" : 5,
                "name" : "Mustanaamio",
                "author" : "mikko",
                "read": true
            } )
            .expect(200, done);
    });   

    // Incorrect parameters
    it("Put : /api/v1/books/1", (done) => {
        request(index)
            .put("/api/v1/books/1")
            .send( {
                "id" : 5,
                "name" : "Mustanaamio",
                "author" : "mikko"
            } )
            .expect(400, done);
    });  

    // No such book
    it("Put : /api/v1/books/15", (done) => {
        request(index)
            .put("/api/v1/books/15")
            .send( {
                "id" : 5,
                "name" : "Mustanaamio",
                "author" : "mikko",
                "read": true
            } )
            .expect(400, done);
    });  


    // DELETE
    // HAPPY CASE 
    it("Post : /api/v1/books/0", (done) => {
        request(index)
            .delete("/api/v1/books/0")
            .expect(200, done);
    });  

    // NO SUCH BOOK
    it("Post : /api/v1/books/12", (done) => {
        request(index)
            .delete("/api/v1/books/12")
            .expect(400, done);
    });  
    
    // Static
    it("Test : / : returns 200", (done) => {
        request(index)
            .get("/")
            .expect(200, done);
    });

    it("Test : /somelostpoint : returns 404", (done) => {
        request(index)
            .get("/somelostpoint")
            .expect(404, done);
    });


    
});


