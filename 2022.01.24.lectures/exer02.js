/**
 * Expand the API from Assignment 1 by accepting a name
through the counter endpoint:
/counter/:name
When entering this endpoint from the browser, the names
and the count of how many times the endpoint was
entered should be shown as follows:
Rene was here 3 times
Peetu was here 999 times
 */

import express from "express";

const app = express();

const port = 3000;

// Browser : http://localhost:3000/counter/annaliisa
// or
// Browser : http://localhost:3000/counter/maija


let counter = {};

app.get("/counter/:name", (request,response) => {
    
    const name = request.params.name;
    let count = counter[name] || 1;

    counter[name] = count;
    
    response.send(`<h1>${name} was here ${counter[name]++} times.</h1>` );
});


app.listen(port,  ()  => {
    console.log (`Listening to port ${port}`);
});
