/**
Exercise 4: Student registry
Create an API that allows you to GET a single student from and POST
student into a registry.
GET /student/:id
POST /student/
Through the POST request, you should store the name, id and email
address of the student.
When you GET the student, you should display these details as JSON.
Extra 1: Create PUT and DELETE endpoints for the student as well.
 */

// Postman : http://localhost:3000/student:id


import express from "express";

const app = express();
app.use (express.json()); // this must be used to parse POST request body made in JSON format (se in Postman)

const port = 3000;


const keys = ["id", "name", "email"];
const students = [
    {"id": 0, "name" : "Matthew", "email" : "matthew@gmail.com"},
    {"id": 1, "name" : "Mike", "email" : "mike@hotmail.com"}  
]   ;



app.get("/student/:id", (req,res) => {
    
    console.log("GET request init!");
    const reqID = Number(req.params.id);

    console.log(`Requested id : ${reqID}.`);

    const data = students.filter( element => element.id === reqID );

    if (data.length !== 0) {
        res.status(200).send(data);
    } else {
        res.status(400).send(`Student with ID ${reqID} not found.`);
    }
});


app.post("/student", (req,res) => {
    
    console.log("POST request init!");
    const studentData = req.body;
    if (!keys.every(key => studentData.hasOwnProperty(key))) {
        res.status(400).send(`Please provide following data : ${keys}.`); 
    } else {
        students[students.length] = studentData;
        console.log(students);
    
        res.status(200).send("Data saved");    
    }

});

app.delete("/student/:id", (req,res) => {
    
    console.log("DELETE request init!");

    const reqID = Number(req.params.id);

    let delIndex = undefined;
    students.find (   (value,index,array) => {
        if (array[index].id === reqID) {
            delIndex = index;
        }}
    );
    if (delIndex !== undefined) {
        students.splice(delIndex,1);
        res.status(200).send(`Student record with ID ${reqID} deleted.`);    
    } else {
        res.status(400).send(`Student record with ID ${reqID} not found.`);    
    }
    
    console.log(students);

});

// updates a student record
app.put("/student/:id", (req,res) => {
    
    console.log("PUT request init!");
    const reqID = Number(req.params.id);

    const studentData = req.body;
    let studentFound = false;
    
    if (!keys.every(key => studentData.hasOwnProperty(key))) {
        res.status(400).send(`Please provide following data : ${keys}.`); 
    } else {
        
        console.log(`Requested id : ${reqID}.`);
        students.find (   (value,index,array) => {
            if (array[index].id === reqID) {
                studentFound = true;
                array[index] = studentData;
            }}
        );
    }

    if (studentFound) {
        res.status(200).send(`Student with ID ${reqID} updated.`);
    } else {
        res.status(400).send(`Student with ID ${reqID} not found.`);
    }
    
    console.log(students);
});


app.listen(port,  ()  => {
    console.log (`Listening to port ${port}`);
});
