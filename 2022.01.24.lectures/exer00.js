/**
 * Create two endpoints with Express. Endpoint respond to
GET requests.
First endpoint is /hello, which responds with “world!”
Second endpoint is /foo, which responds with “bar”
 */

import express from "express";

const app = express();

const port = 3000;

// Browser : http://localhost:3000/hello
// Browser : http://localhost:3000/foo

app.get("/hello", (request,response) => {
    response.send("world!");
});

app.get("/foo", (request,response) => {
    response.send("bar");
});

app.listen(port,  ()  => {
    console.log (`Listening to port ${port}`);
});

