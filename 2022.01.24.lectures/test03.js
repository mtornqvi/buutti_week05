import express from "express";

const app = express();

const port = 3000;

// Browser : http://localhost:3000/John?param=parametri
/**Params = 
{ name: 'John' }
Query =
{ param: 'parametri' } 
 * 
 */
app.get("/name", (request,response) => {
    console.log("Params = ");
    console.log(request.params);
    console.log("Query = ");
    console.log(request.query);
    response.send("Hello world!");
});

app.listen(port,  ()  => {
    console.log (`Listening to port ${port}`);
});

