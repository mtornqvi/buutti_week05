const reqID = 4;
const keys = ["id", "name", "email"];
const students = [
    {"id": 3, "name" : "Matthew", "email" : "matthew@gmail.com"},
    {"id": 4, "name" : "Mike", "email" : "mike@hotmail.com"}  
];

const studentData = {"id": 4, "name" : "Sanna", "email" : "sanna@hotmail.com"};

students.find (   (value,index,array) => {
    if (array[index].id === reqID) {
        array[index] = studentData;
    }}
);

console.log(students);