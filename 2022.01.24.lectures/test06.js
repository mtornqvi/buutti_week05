const reqID = 4;
const keys = ["id", "name", "email"];
const students = [
    {"id": 3, "name" : "Matthew", "email" : "matthew@gmail.com"},
    {"id": 4, "name" : "Mike", "email" : "mike@hotmail.com"}  
];

let data = students[0];
console.log(data);

data = students.filter( element => element.id === reqID);
console.log(data);

let delIndex;
students.find (   (value,index,array) => {
    if (array[index].id === reqID) {
        delIndex = index;
    }}
);
students.splice(delIndex,1);

console.log(students);
