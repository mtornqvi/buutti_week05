import express from "express";
import dotenv from "dotenv";
import "express-async-errors";
import productRouter from "./api/productRouter.js";
import userRouter from "./api/userRouter.js";
import protectedRouter from "./api/protectedRouter.js";
import db from "./db/db.js";

if (process.env.NODE_ENV === "dev") {
    dotenv.config();
}

const app = express();
app.use(express.json());
app.use("/", productRouter);
app.use("/user", userRouter);
app.use("/protected", protectedRouter);
db.createTables();

const unknownEndpoint = (_req, res) => {
    res.status(404).send({ error: "No one here" });
};

const errorHandler = (error, _req, res, next) => {
    if (error.name === "JsonWebTokenError") {
        res.status(401).send({ error: "unauthorized" });
    } else if (error.code === "23505") {
        return res.status(400).send({ error: "Please try another username" });
    }
    next(error);
};
  
app.use(unknownEndpoint);
app.use(errorHandler);

const APP_PORT = process.env.APP_PORT || 8080;

app.listen(APP_PORT,() => {
    console.log(`Listening to ${APP_PORT}.`);
});