import { Router } from "express";
import productService from "../services/productService.js";

const router = Router();

router.get("/", (_req, res) => {
    res.send("OK");
});

router.post("/product", async (req, res) => {
    const product = req.body;
    const result = await productService.insertProduct(product);
    res.status(result ? 201 : 500).send(result ? "Created" : "Error");
});

router.get("/products", async (_req, res) => {
    const products = await productService.findAll();
    res.status(200).send(products);
});

export default router;