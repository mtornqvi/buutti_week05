import { Router } from "express";
import jwt from "jsonwebtoken";

const router = Router();

const extractToken = (header) => {
    if (header && header.toLowerCase().startsWith("bearer ")) {
        const token = header.substring(7);
        return token;
    }
};

router.use((req, res, next) => {
    const token = extractToken(req.get("authorization"));
    const decodedToken = jwt.verify(token, process.env.APP_SECRET);
    if (!token || !decodedToken.username) {
        console.log(decodedToken.username);
        res.status(401).send("Token missing or invalid");
    } else {
        next();
    }
});

router.get("/", async (req, res) => {
    res.status(200).send("Hello from protected router!");
});

export default router;