import pg from "pg";
import dotenv from "dotenv";
import queries from "./queries.js";

pg.defaults.ssl = true;
const isDev = process.env.NODE_ENV === "dev";

if (isDev) {
    dotenv.config();
}

const { APP_PG_HOST, APP_PG_USER, APP_PG_DB, APP_PG_PASSWORD, APP_PG_PORT } = process.env;

const pool = new pg.Pool({
    host: APP_PG_HOST,
    user: APP_PG_USER,
    database: APP_PG_DB,
    password: APP_PG_PASSWORD,
    port: APP_PG_PORT,
    ssl: !isDev
});

// Parameters are expected to be passed as an array of primitives
const executeQuery = async (query, parameters) => {
    const client = await pool.connect();
    try {
        const result = await client.query(query, parameters);
        return result;
    } catch (error) {
        console.error(error.stack);
        throw error;
    } finally {
        client.release();
    }
};

const createTables = async () => {
    const result = await Promise.all([
        await executeQuery(queries.createProductTable),
        await executeQuery(queries.createCustomerTable)
    ]);
    console.log(result ? 
        "Tables created (or previously existed) successfully." : 
        "Error in initializing database"
    );
};

export default { 
    executeQuery,
    createTables
};