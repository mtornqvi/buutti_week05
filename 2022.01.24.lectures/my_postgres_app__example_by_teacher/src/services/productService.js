import productDao from "../dao/productDao.js";

const insertProduct = async (product) => {
    const result = await productDao.insertProduct(product);
    return result;
};

const findAll = async () => {
    const products = await productDao.findAll();
    return products.rows;
};

export default { findAll, insertProduct };