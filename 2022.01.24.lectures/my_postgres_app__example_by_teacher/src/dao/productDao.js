import { v4 as uuidv4 } from "uuid";
import db from "../db/db.js";

const insertProduct = async (product) => {
    const params = [uuidv4(), ...Object.values(product)];
    console.log(`Inserting a new product ${params[0]}...`);
    const result = await db.executeQuery(
        "INSERT INTO \"products\" (\"id\",\"name\",\"price\") VALUES ($1, $2, $3);", 
        params
    );
    console.log(`New product ${params[0]} inserted successfully.`);
    return result;
};

const findAll = async () => {
    console.log("Requesting for all products...");
    const result = await db.executeQuery(
        "SELECT * FROM \"products\";"
    );
    console.log(`Found ${result.length} products.`);
    return result;
};

export default {
    insertProduct,
    findAll
};