import express from "express";


const app = express();

const port = 3000;


const counter = {};

app.use("/counter/:name", (req,res,next) => {

    
    console.log("hello from middleware!");

    if(req.params.name === "joel") {
        res.status(401).send("Ei käy!");
    } else {
        next();
    }

});

app.get("/counter/:name", (request,response) => {
    
    console.log("Hello from endpoint!");

    const name = request.params.name;
    let count = counter[name] || 1;

    counter[name] = count;
    
    response.send(`<h1>${name} was here ${counter[name]++} times.</h1>` );
});


app.listen(port,  ()  => {
    console.log (`Listening to port ${port}`);
});
