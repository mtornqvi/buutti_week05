/**
 * Create an API that consists of only one endpoint:
/counter
Whenever you enter this endpoint from the browser, it
should show you how many times the endpoint has been
called, inside a h1 header.
Also, Make it possible to set the counter to whichever
integer with a query parameter /counter?number=5
 */

import express from "express";

const app = express();

const port = 3000;

// Browser : http://localhost:3000/counter
// or
// Browser : http://localhost:3000/counter?number=3


let counter = 1;

app.get("/counter", (request,response) => {
    
    if (request.query.number !== undefined) {
        counter = Number(request.query.number); // must have Number()
    }
    
    response.send(`<h1> This endpoint has been called ${counter++} times.</h1>`);
});



app.listen(port,  ()  => {
    console.log (`Listening to port ${port}`);
});

