/**
 * 1. Create an API, which accepts POST -requests and
console.logs a variable set in the request.body
2. Create a POST request with Postman to test your
newly created API
3. Verify that the endpoint works as expected.
 */

import express from "express";

const app = express();
app.use (express.json()); // this must be used to parse POST request body made in JSON format (se in Postman)

const port = 3000;

// Postman : http://localhost:3000

app.post("/", (req,res) => {
    
    console.log("POST request init!");
    console.log(req.body);
    res.status(200).send("OK!");
});


app.listen(port,  ()  => {
    console.log (`Listening to port ${port}`);
});
